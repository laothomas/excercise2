package com.example.shaker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shaker.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager manager;
    private Sensor acc;
    private long lastUpdate = System.currentTimeMillis();
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        manager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
            acc = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        else {
            binding.result.setText("Missing accelerometer");
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (acc != null)
            manager.registerListener(this, acc, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onStop() {
        super.onStop();
        manager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            // Your code here
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];
            String result = String.format("Acceleration:\n" + "X axis: %.5f\nY axis: %.5f\nZ axis: %.5f",
                    x, y, z);
            binding.result.setText(result);

            float accSquareRoot = (x * x + y * y + z * z)
                    / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
            long actualTime = System.currentTimeMillis();
            if (accSquareRoot >= 2 && actualTime - lastUpdate > 200) {
                lastUpdate = actualTime;
                Toast.makeText(this, "Don't shake me!", Toast.LENGTH_LONG).show();
                binding.text.setBackgroundColor(Color.RED);
            } else binding.text.setBackgroundColor(Color.BLUE);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {


    //
//    // TextViews to display current sensor values
//    private TextView mTextSensorLight;
//    private TextView mTextSensorProximity;
    private SensorManager mSensorManager;

    private Sensor mSensorProximity;
    private Sensor mSensorLight;
    private TextView mTextSensorLight;
    private TextView mTextSensorProximity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //    // Individual light and proximity sensors.
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mTextSensorLight = (TextView) findViewById(R.id.labellight);
        mTextSensorProximity = (TextView) findViewById(R.id.labelproximity);
        mSensorProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mSensorLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        String sensor_error = getResources().getString(R.string.error_no_sensor);
        if (mSensorLight == null) {
            mTextSensorLight.setText(sensor_error);
        }
        if (mSensorProximity == null) {
            mTextSensorProximity.setText(sensor_error);
        }


    }

    /*
     * do not register sensor listeners inside onCreate. listening to sensor data uses up battery.
     *
     * Use onStart() and onStop() to ensure that sensors continue running even if the app is in multi-window mode.
     *
     * Each sensor your app needs requires a listener provided by the Sensor Manager
     *
     */
    @Override
    protected void onStart() {
        super.onStart();
        if (mSensorProximity != null) {
            //use these registerListeners for whatever sensors you want
            mSensorManager.registerListener(this, mSensorProximity, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mSensorLight != null) {
            mSensorManager.registerListener(this, mSensorLight, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    protected void onStop() {
        super.onStop();
        mSensorManager.unregisterListener(this);
    }

    @Override
    //event stores sensor data in a value arrays
    public void onSensorChanged(SensorEvent event) {

        //get the sensor type
        /*
         * Called with a SensorEvent object
         */
        int sensorType = event.sensor.getType();
        float currentValue = event.values[0];
        switch (sensorType) {
            case Sensor.TYPE_LIGHT:
                //handle light sensor
                mTextSensorLight.setText(getResources().getString(R.string.label_light, currentValue));
                break;
            case Sensor.TYPE_PROXIMITY:
                mTextSensorProximity.setText(getResources().getString(R.string.label_proximity, currentValue));
            default:
                //do nothing
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}